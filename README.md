## Code for my "Webapps with Angular" on the NFJS tour

A simple starter project demonstrating the basic concepts of Angular.


### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed version 5+
- Make sure you have NPM installed version 3+
- `WINDOWS ONLY` run `npm install -g webpack webpack-dev-server typescript` to install global dependencies
- Install [`angular-cli`](https://github.com/angular/angular-cli)
- run `npm install` to install dependencies
- In one terminal run `node server.js`
- In another terminal, run `ng serve -pc proxy.config.json` to fire up dev server
- open browser to [`http://localhost:4200`](http://localhost:4200)

### Theme

Thanks to [modularcode/modular-admin-html](https://github.com/modularcode/modular-admin-html)

#### Caveats

`modular-admin-html` dynamcially switches the CSS based on the "Customize" menu that displays in the homepage.
However, `angular-cli` does not support this with some hackery.
In this version clicking on any of the colors in the "Customize" menu in the app will not do anything.
I promise to look into this soon.
