import { Routes } from '@angular/router';

import { ContainersComponent } from './containers';
import { DashboardComponent } from './dashboard';

export const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'containers',
    component: ContainersComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];
