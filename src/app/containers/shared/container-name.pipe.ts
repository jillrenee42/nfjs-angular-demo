import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'containerName'
})
export class ContainerNamePipe implements PipeTransform {

  transform(container: any, args?: any): any {
    const names = container.Names[0];

    return names.substring(1, names.length);
  }

}
