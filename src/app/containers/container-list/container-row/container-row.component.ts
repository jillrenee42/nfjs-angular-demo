import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tr.container-row',
  templateUrl: './container-row.component.html',
  styleUrls: ['./container-row.component.css']
})
export class ContainerRowComponent implements OnInit {
  @Input() container;
  constructor() { }

  ngOnInit() {
  }

}
