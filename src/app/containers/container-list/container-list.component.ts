import { Component, OnInit } from '@angular/core';
import { ContainerService } from '../shared';

@Component({
  selector: 'app-container-list',
  templateUrl: './container-list.component.html',
  styleUrls: ['./container-list.component.css']
})
export class ContainerListComponent implements OnInit {
  containers: Array<any>;

  constructor(private containerService: ContainerService) { }

  ngOnInit() {
    this.containerService.getContainers()
      .subscribe(conts => this.containers = conts);
  }

}
